defmodule CommentsWeb.Router do
  use CommentsWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", CommentsWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  scope "/", CommentsWeb do
    pipe_through :api

    resources "/comments", CommentsController, only: [:index, :create]
  end


  # Other scopes may use custom stacks.
  # scope "/api", CommentsWeb do
  #   pipe_through :api
  # end
end
