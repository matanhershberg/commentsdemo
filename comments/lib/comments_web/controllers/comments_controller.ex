defmodule CommentsWeb.CommentsController do
  use CommentsWeb, :controller

  def create(conn, %{"comment" => comment} = params) do
    {:ok, result} = Comments.Repo.insert(%Comments.Comment{comment: comment})

    conn
    |> put_status(:created)
    |> json(result)
  end

  def index(conn, _params) do
    result = Comments.Repo.all(Comments.Comment)
    json(conn, result)
  end
end
