defmodule Comments.Repo do
  use Ecto.Repo,
    otp_app: :comments,
    adapter: Ecto.Adapters.Postgres
end
