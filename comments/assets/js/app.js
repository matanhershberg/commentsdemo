// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.css"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"

import '../semantic/dist/semantic'

import Vue from '../node_modules/vue/dist/vue'
import Body from './Body.vue'

new Vue({
  el: '#app',
  template: '<Body/>',
  components: { Body }
})
