# Comments Demo for CarLabs

# Run

The easiest way to run this is using docker.
Run `docker-compose up` from the root directory.

Then access it from the browser at `http://localhost`

# pgAdmin

To access the database gui go to `http://localhost:81`.

```
User: matanhershberg@gmail.com
Pass: dev
```
